using CairoMakie

function plots(state, parameters, angularFrequency, timespan)
    N, J, K = parameters
    N = Int(N)
    outputMembers = N - 1

    x = state[:, 1:N]
    theta = mod.(state[:, N+1:end], 2pi)

    i = 1
    f1 = plotX(x[:, i], timespan, parameters, angularFrequency)
    display(f1)

    f2 = plotTheta(theta[:, i], timespan, parameters, angularFrequency)
    display(f2)

end


function plotX(x, timespan, parameters, angularFrequency)

    time = 1:timespan
    inputX = 3*sin.(angularFrequency*time)

    f = Figure()
    ax = Axis(f[1, 1],
        title = "Amplitude",
        xlabel = "Time",
        ylabel = "X"
    )
    l1 = lines!(ax, time, x)
    l2 = lines!(ax, time, inputX)
    # Legend(f, [l1, l2], ["Output", "Input"])
    return f
end


function plotTheta(theta, timespan, parameters, angularFrequency)

    time = 1:timespan
    inputTheta = mod.(angularFrequency*time, 2pi)

    f = Figure()
    ax = Axis(f[1, 1],
        title = "Phase",
        xlabel = "Time",
        ylabel = "Theta"
    )
    lines!(ax, time, theta)
    lines!(ax, time, inputTheta)
    return f
end