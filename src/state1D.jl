"""
State Function to  be used in the difference equation. It implements the 1D model
Input: 
 - state: 1x2N vector, x -> state[1:N], theta -> state[N+1:end]
 - parameters: [N, J, K]
 - timestep: current timestep
 - angular frequency (in rads)
 - codependence: Boolean variable, decides whether the codepedentent or the idependent model will be used

Output:
 - dstate: the next state
"""
function state1D(state, parameters, timestep, angularFrequency, codependence = true)

    N = Int(parameters[1])

    x = state[1:N]    
    theta = state[N + 1:end]

    inputX = 3*sin(angularFrequency*timestep)
    inputTheta = angularFrequency*timestep

    stateArgs = (x, theta, parameters, angularFrequency)

    if codependence
        return codependentState1D(stateArgs...)
    else
        return independentState1D(stateArgs...)
    end
end


"""
Function that implements the 1D state where every member of the state acts on one another, on top of the input member
Input: 
 - x: 1xN vector
 - theta: 1xN vector
 - parameters: [N, J, K]
 - angular frequency (in rads)

Output:
 - dstate: the next state
"""
function codependentState1D(x, theta, parameters, angularFrequency)

    N, J, K = parameters
    N = Int(N)

    dx = zeros(AbstractFloat, 1, N)
    dtheta = zeros(AbstractFloat, 1, N)

    for i = 1:N-1
        sumX = 0
        sumTheta = 0
        for j = 1:N
            if i != j
                calculateArgs = (x[i], x[j], theta[i], theta[j])
                sumX += calculateX(calculateArgs..., J)
                sumTheta += calculateTheta(calculateArgs...)
            end
        end
        dx[i] = sumX/N
        dtheta[i] = angularFrequency + K*sumTheta/N
    end
    return hcat(dx, dtheta)
end


"""
Function that implements the 1D state where only the input member acts (x[N], theta[N]) on each other member
Input: 
 - x: 1xN vector
 - theta: 1xN vector
 - parameters: [N, J, K]
 - angular frequency (in rads)

Output:
 - dstate: the next state
"""
function independentState1D(x, theta, parameters, angularFrequency)

    N, J, K = parameters
    N = Int(N)

    dx = zeros(AbstractFloat, 1, N)
    dtheta = zeros(AbstractFloat, 1, N)
    
    for i = 1:N-1
        calculateArgs = (x[i], x[N], theta[i], theta[N])
        dx[i] = 1/N * calculateX(calculateArgs..., J)
        dtheta[i] = angularFrequency + K / N * calculateTheta(calculateArgs...)
    end
    return hcat(dx, dtheta)
end


"""
Function that calculates the value of x for the current member combined with another member
Input: 
 - xExamined: the value of x of the current member 
 - inputX: the value of x of the input
 - thetaExamined: the value of theta of the current member 
 - inputTheta: the value of theta of the input
 - J: attractive factor

Output:
 - value of the calculation
"""
function calculateX(xExamined, inputX, thetaExamined, inputTheta, J)  
    return (inputX - xExamined) * (1 + J*cos(inputTheta - thetaExamined))/abs(inputX - xExamined) - (inputX - xExamined)/(inputX - xExamined)^2
end

"""
Function that calculates the value of theta for the current member combined with another member
Input: 
 - xExamined: the value of x of the current member 
 - inputX: the value of x of the input
 - thetaExamined: the value of theta of the current member 
 - inputTheta: the value of theta of the input

Output:
 - value of the calculation
 """
function calculateTheta(xExamined, inputX, thetaExamined, inputTheta)
    return sin(inputTheta - thetaExamined)/abs(inputX - xExamined)
end