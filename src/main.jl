include("./state1D.jl")
include("./plots.jl")
# using DifferentialEquations

function simulation1D(N, J, K, angularFrequency)
    parameters = (N, J, K)
    timespan = 100

    x = zeros(Float64, timespan, N)
    theta = zeros(Float64, timespan, N)
    state = hcat(x, theta)
    state[1, :] = rand(Float64, 1, 2*N)

    for timestep = 1:timespan-1
        currentState = state[timestep,:]
        dState = state1D(currentState, parameters, timestep, angularFrequency)
        state[timestep+1, :] = dState' + currentState
    end

    plots(state, parameters, angularFrequency, timespan)

end




simulation1D(2, 0.8, -1, 1)